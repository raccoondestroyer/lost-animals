var express = require('express');
var router = express.Router();
var dbConn = require('../config/db');

/**
 * Показать всех животных из бд (от самой последней записи)
 */
router.get('/', function (req, res, next) {
  dbConn.query('SELECT * FROM animals ORDER BY id desc', function (error, rows) {
    if (error) {
      req.send('error', error);
      res.render('animals', { data: '' });
    } else {
      res.render('animals', { data: rows });
    }
  })
})


/**
 * новая запись
 */
router.get('/add', function(req, res, next) {    
  res.render('animals/add', {
      name: ''  
  })
})

/**
 * добавить
 */
router.post('/add', function(req, res) {
  let name = req.body.name;
  let errors = false;

  if(name.length === 0) {
      errors = true;
      console.log("Нужно ввести название");
      res.render('animals/add', {
          name: name
      })
  }

  if(!errors) {
    let form_data = {
      name: name
    };

    // insert запрос
      dbConn.query('INSERT INTO animals SET ?', form_data, function(err, result) {
        if (err) {
          console.log('error', err)
          res.render('animals/add', {
              name: form_data.name
          })
        } else {
          console.log('Запись добавлена')
          res.redirect('/animals')
        }
      })
  }
})

/**
 * Изменить запись
 */
router.get('/edit/(:id)', function (req, res, next) {
  let id = req.params.id;
  
  dbConn.query('SELECT * FROM animals WHERE id = ' + id, function (err, rows, fields) {
    if (err) throw err

    if (rows.length <= 0) {
      console.log('Не найдено id = ' + id)
      res.redirect('/animals')
    }
    else {
      res.render('animals/edit', {
        title: 'Изменить запись',
        id: rows[0].id,
        name: rows[0].name
      })
    }
  })
})


router.post('/update/:id', function (req, res, next) {
  let id = req.params.id;
  let name = req.body.name;
  let errors = false;

  //проверка ввода названия 
  if (name.length === 0) {
    errors = true;

    req.send("Введите название");
    res.render('animals/edit', {
      id: req.params.id,
      name: name
    })
  } 

  //если без ошибок
  if (!errors) {
    var form_data = {
      name: name
    }
    
    // update запрос
    dbConn.query('UPDATE animals SET ? WHERE id = ' + id, form_data, function (err, result) {
      if (err) {
        req.send(err)
        res.render('animals/edit', {
          id: req.params.id,
          name: form_data.name,
        })
      } else {
        console.log('Изменено');
        res.redirect('/animals');
      }
    })
  }
})


/**
 * Удаление записей
 **/ 
router.get('/delete/(:id)', function (req, res, next) {
  let id = req.params.id;

  dbConn.query('DELETE FROM animals WHERE id = ' + id, function (err, result) {
    if (err) {
      res.redirect('/animals')
    } else {
      console.log('Запись была удалена! ID = ' + id)
      res.redirect('/animals')
    }
  })
})

/**
 * Вывод на печать
 */
router.get('/print/(:id)', function(req, res, next) {    
  res.render('animals')
})

module.exports = router;