var mysql = require('mysql');

//доступы к бд
var connection = mysql.createConnection({
	host:'localhost',
	user:'root',
	password:'',
	database:'express'
});

// открываем соединение с базой данных
connection.connect(function(error) {
	if(error) {
		console.log(error); //ошибки
	} else {
		console.log('Успешно соединено с базой данных!');
	}
});

module.exports = connection; //экспортируем наше соединение